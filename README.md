Nome | Data | Revisado |
---------------------------- | -------------------------------------- | ----------------------------------------
José R. F. Júnior            | 13/03/2020                             | -

# Maior Número Família
A classe responsável em verificar e retornar o maior número de uma família de N está em:
```br.com.navita.app.core.application.dto.familia.FamiliaNumeroService```

E sua classe de test encontra-se em:
test ```br.com.navita.app.core.application.dto.familia.FamiliaNumeroTest```


# API REST NAVITA

## 1. Resumo
A APIREST fornece recursos acessíveis a os desenvolvedores por meio de uma interface HTTP RESTFUL. 

- [1. Resumo](#1-resumo)
- [2. - Instruções para execução](#2-instruções-para-execução)
- [3. - Métodos Suportados](#3-métodos-suportados)
- [4. - Endpoints](#4-endpoints)
- [5. - Token de Acesso](#5-token-de-acesso)
- [6 - Solução Adotada](#6-solucao-adotada)

## 2. Instruções para execução
Crie um banco de dados posttgreSQL com o nome ```navita```

## 3. Métodos Suportados
As operações devem usar os métodos HTTP adequados.

Abaixo está uma listas de métodos que os serviços REST da Navita devem suportar. 
Nem todos os recursos suportam todos os métodos, mas todos os recursos usando os métodos abaixo devem estar em conformidade com seu uso

Method  | Description                                                                                                                
------- | ------------------------------------------------------------------------------------------------------
GET     | Retorna o valor atual de um objeto                                                                                     
PUT     | Altera um objeto                                                              
DELETE  | Deleta um objeto                                                                                                      
POST    | Cria um noov objeto com baso nos dados fornecidos       

## 4. Endpoints

##### Usuário
```
	POST /usuario cria um novo usuário
	GET /usuario retorna todos os usuários *
	DELETE /usuario/{id} deleta um usuário *
```	

##### Marca
```
	GET /marca retorna todas as marcas *
	GET /marca/{id} retorna uma marca por id *
	GET /marca/{id}/patrimonios retorna todos os patrimônios de uma marca
	POST /marca cria uma nova marca *
	PUT /marca/{id} altera uma marca *
	DELETE /marca/{id} deleta uma marca *
```	

##### Patrimônio
```
	GET /patrimonio retorna todos os patrimônios *
	GET /patrimonio/{id} retorna um patrimônio por id *
	POST /patrimonio cria um novo patrimônio *
	PUT /patrimonio/{id} altera um patrimônio *
	DELETE /patrimonio/{id} deleta um patrimônio *
```	

##### Autenticar
```
	POST /autenticar retorna os dados do usuário autenticado 
```	

### * é necessário token de autenticação

Respeitando o padrão REST, toda vez que um POST for acionado, o status 201 será enviado e um header Location terá o caminho para encontrar o objeto salvo

```http
201 Created
Location: https://localhost/marca/1
```     

Os recursos podem ser acessados via swagger ```http://localhost:8080/swagger-ui.html```      

## 5. Token de Acesso
O retorno do endpoint ```http /autenticar``` contém:

```json
	id: '',
	nome: '',
	token: {
		token: '',
		tipo: ''
	}
```

O Token de Acesso será recebido através do header Authorization, exemplo:

```Authorization → {token,tipo} {token.token}```
                                                        
## 6. Solução Adotada

- Arquitetura Hexagonal, onde enfatizamos uma camada em especial, na qual ficará toda a lógica principal da aplicação(core)
- Java 8
- SpringBoot
- Swagger
- SpringSecurity usando JWT
- OWASP para validação de dados maliciosos informados pelo usuário
- Flyway
- Gradle
- Git