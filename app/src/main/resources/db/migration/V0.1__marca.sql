CREATE TABLE marca
(
    id uuid NOT NULL,
    nome character varying(150) NOT NULL,
    dt_criacao timestamptz NOT NULL,
    dt_ultima_alteracao timestamptz  NOT NULL,
    versao integer NOT NULL,
    CONSTRAINT marca_pkey PRIMARY KEY (id)
)