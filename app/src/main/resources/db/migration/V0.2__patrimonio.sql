CREATE TABLE patrimonio
(
    id uuid NOT NULL,
    marca_id uuid NOT NULL,
    nome character varying(75),
    descricao character varying(2500),
    numero_tombo bigint NOT NULL,
    dt_criacao timestamptz NOT NULL,
    dt_ultima_alteracao timestamptz  NOT NULL,
    versao integer NOT NULL,
    CONSTRAINT patrimonio_pkey PRIMARY KEY (id),
    CONSTRAINT marca_fkey FOREIGN KEY (marca_id) REFERENCES marca (id)
)