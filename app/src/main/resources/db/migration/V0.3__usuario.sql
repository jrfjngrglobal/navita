CREATE TABLE usuario
(
    id uuid NOT NULL,
    nome character varying(75) NOT NULL,
    email character varying(120)  NOT NULL,
    senha character varying(255)  NOT NULL,
    dt_criacao timestamptz NOT NULL,
    dt_ultima_alteracao timestamptz  NOT NULL,
    versao integer NOT NULL,
    CONSTRAINT usuario_pkey PRIMARY KEY (id)
);

CREATE TABLE usuario_funcao
(
    id uuid NOT NULL,
    funcao character varying(80) NOT NULL,
    usuario_id uuid,
    dt_criacao timestamptz NOT NULL,
    dt_ultima_alteracao timestamptz  NOT NULL,
    versao integer NOT NULL,
    CONSTRAINT usuario_funcao_pkey PRIMARY KEY (id),
    CONSTRAINT usuario_fkey FOREIGN KEY (usuario_id) REFERENCES usuario (id)
);

