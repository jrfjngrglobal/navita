package br.com.navita.app.adapters.rest.exception.payload;

import br.com.navita.app.core.application.commons.enums.ECodigoErro;
import br.com.navita.app.core.application.exeption.NavitaException;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class ErroApi {

    private String parametro;
    private String valor;
    private String mensagem;
    private String codigo;

    public ErroApi(final String value, final String code){
        this.valor = value;
        this.codigo = code;
    }

    public ErroApi(final String parameter, final String value,
                   final String message, final ECodigoErro eCodigoErro) {
        this.parametro = parameter;
        this.valor = value;
        this.mensagem = message;
        this.codigo  = eCodigoErro.name();
    }

    public ErroApi(final NavitaException exception){
        this.parametro = exception.getParametro();
        this.valor = exception.getValor();
        this.mensagem = exception.getMensagem();
        this.codigo  = exception.getCodigoErro().name();
    }

    public String getParametro() {
        return parametro;
    }

    public void setParametro(String parametro) {
        this.parametro = parametro;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
}
