package br.com.navita.app.core.application.dto.familia;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class FamiliaNumeroService {

    private static final Long MAIOR_QUE = Long.valueOf(100000000);

    public Long obterMaiorNumeroDaFamilia(final Long numero) throws Exception {

        if(Boolean.TRUE.equals(numero <= Long.valueOf(10)))
            throw new Exception("Número inválido");

        final Long primeiroIrmao = inverter(numero);
        if(Boolean.TRUE.equals(primeiroIrmao.equals(numero)))
            return numero;

        List<Long> familia = new ArrayList<>(Arrays.asList(numero, primeiroIrmao));

        Boolean next = true;
        while(next){
            Long n = inverter(familia.get(familia.size() - 1));

            if(Boolean.FALSE.equals(n.equals(numero)))
                familia.add(n);
            else
                next = Boolean.FALSE;
        }

        final Long maiorNumero = familia.stream().mapToLong(i -> i).max().getAsLong();
        return maiorNumero > MAIOR_QUE ? -1 : maiorNumero;
    }

    private Long inverter(final Long numero){
        final String string = String.valueOf(numero);
        final int length = string.length();
        return Long.valueOf(string.substring(length - 1,length).concat(string.substring(0, length - 1)));
    }
}
