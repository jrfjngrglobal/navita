package br.com.navita.app.core.application.form;

import br.com.navita.app.core.commons.validadores.string.NavitaString;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

public class UsuarioRequestBody implements Serializable {

    @NavitaString(required = true, fieldName = "nome", minLength = 3, maxLength = 50)
    @ApiModelProperty(notes = "Nome do Usuário", required = true)
    private String nome;

    @NavitaString(required = true, fieldName = "email", isEmail = true, minLength = 5, maxLength = 70)
    @ApiModelProperty(notes = "Email do Usuário", required = true)
    private String email;

    @NavitaString(required = true, fieldName = "senha", minLength = 8, maxLength = 16)
    @ApiModelProperty(notes = "Senha do Usuário", required = true)
    private String senha;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
}