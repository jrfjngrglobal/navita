package br.com.navita.app.core.application.commons.helpers;

import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

@Component
public class MessageHelper {

    private final MessageSource messageSource;

    public MessageHelper(final MessageSource messageSource){
        this.messageSource = messageSource;
    }

    public String getMessage(final String key, final Object... params){
        return messageSource.getMessage(key, params, LocaleContextHolder.getLocale());
    }

    public String getMessage(final String key){
        return messageSource.getMessage(key, new Object[0], LocaleContextHolder.getLocale());
    }
}
