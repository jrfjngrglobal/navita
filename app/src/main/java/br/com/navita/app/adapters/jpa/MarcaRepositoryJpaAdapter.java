package br.com.navita.app.adapters.jpa;

import br.com.navita.app.core.domain.marca.Marca;
import br.com.navita.app.core.domain.marca.MarcaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

@Repository
public class MarcaRepositoryJpaAdapter {

    private final MarcaRepository repository;

    public MarcaRepositoryJpaAdapter(final MarcaRepository repository){
        this.repository = repository;
    }

    public Marca salvarOuAlterar(Marca entidade){
        return repository.save(entidade);
    }

    public Collection<Marca> buscarTodas(){
        return repository.findAll();
    }

    public Optional<Marca> buscarPorId(final UUID id){
        return repository.findById(id);
    }

    public Optional<Marca> buscarMarcaPorNome(final String nome){
        return repository.buscarMarcaPorNome(nome);
    }

    public Optional<Marca> buscarMarcaPorNome(final String nome, final UUID diferenteDe){
        return repository.buscarMarcaPorNome(nome, diferenteDe);
    }

    public void deletar(final Marca marca){
        repository.delete(marca);
    }
}