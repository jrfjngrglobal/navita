package br.com.navita.app.core.application.exeption;

import br.com.navita.app.core.application.commons.enums.ECodigoErro;
import com.fasterxml.jackson.annotation.JsonIgnore;

public class NavitaException extends Exception {

    private String parametro;
    private String mensagem;
    private String valor;
    private ECodigoErro codigoErro;

    @JsonIgnore
    private int httpStatusCode;

    public static NavitaException of(final String parametro, final String mensagem, final String valor, final ECodigoErro eCodigoErro){
        NavitaException ex = new NavitaException();
        ex.setParametro(parametro);
        ex.setMensagem(mensagem);
        ex.setValor(valor);
        ex.setCodigoErro(eCodigoErro);
        return ex;
    }

    public static NavitaException of(final String parametro,final String valor, final ECodigoErro eCodigoErro){
        NavitaException ex = new NavitaException();
        ex.setParametro(parametro);
        ex.setValor(valor);
        ex.setCodigoErro(eCodigoErro);
        return ex;
    }

    public String getParametro() {
        return parametro;
    }

    public void setParametro(String parametro) {
        this.parametro = parametro;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public ECodigoErro getCodigoErro() {
        return codigoErro;
    }

    public void setCodigoErro(ECodigoErro codigoErro) {
        this.codigoErro = codigoErro;
    }

    public void setHttpStatusCode(int httpStatusCode) {
        this.httpStatusCode = httpStatusCode;
    }

    public int getHttpStatusCode() {
        return httpStatusCode;
    }
}
