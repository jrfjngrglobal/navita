package br.com.navita.app.core.domain.usuario;

import br.com.navita.app.core.application.commons.enums.EFuncao;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;

@Entity
public class UsuarioFuncao implements GrantedAuthority {

    @Id
    @GeneratedValue
    private UUID id;

    @Enumerated(EnumType.STRING)
    private EFuncao funcao;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "usuario_id", nullable = false)
    private Usuario usuario;

    @CreatedDate
    private LocalDateTime dtCriacao;

    @CreatedDate
    private LocalDateTime dtUltimaAlteracao;

    @Version
    private Integer versao;

    public static UsuarioFuncao of(final EFuncao funcao, Usuario usario){
        UsuarioFuncao entidade = new UsuarioFuncao();
        entidade.setFuncao(funcao);
        entidade.setUsuario(usario);
        return entidade;
    }

    @PrePersist
    protected void onCreate(){
        if(Objects.isNull(this.dtCriacao)){
            this.dtCriacao = LocalDateTime.now();
            this.dtUltimaAlteracao = LocalDateTime.now();
        }
    }

    @PreUpdate
    protected void onUpdate(){
        this.dtUltimaAlteracao = LocalDateTime.now();
    }

    @Override
    public String getAuthority() {
        return funcao.name();
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public void setFuncao(EFuncao funcao) {
        this.funcao = funcao;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Usuario getUsuario() {
        return usuario;
    }
}
