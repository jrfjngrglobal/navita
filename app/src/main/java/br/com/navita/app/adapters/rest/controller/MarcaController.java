package br.com.navita.app.adapters.rest.controller;

import br.com.navita.app.adapters.rest.exception.FormException;
import br.com.navita.app.core.application.dto.marca.MarcaService;
import br.com.navita.app.core.application.dto.patrimonio.PatrimonioDTO;
import br.com.navita.app.core.application.exeption.NavitaException;
import br.com.navita.app.core.application.form.MarcaRequestBody;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.util.Optional;

@CrossOrigin
@RestController
@RequestMapping("/marca")
public class MarcaController {

    private final MarcaService service;

    public MarcaController(final MarcaService service){
        this.service = service;
    }

    @ApiOperation(value = "Buscar Todas Marcas", response = PatrimonioDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Sucesso"),
            @ApiResponse(code = 403, message = "É necessário estar autenticado para acessar o recurso")
    })
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity buscarTodos(){
        return ResponseEntity.ok(service.buscarTodas());
    }

    @ApiOperation(value = "Buscar Marca por ID", response = PatrimonioDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Sucesso"),
            @ApiResponse(code = 404, message = "Patrimônio não encontrado"),
            @ApiResponse(code = 403, message = "É necessário estar autenticado para acessar o recurso")
    })
    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity buscarPorId(@PathVariable String id) throws NavitaException {
        return ResponseEntity.ok(service.buscarPorId(id));
    }

    @ApiOperation(value = "Buscar Patrimônios da Marca", response = PatrimonioDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Sucesso"),
            @ApiResponse(code = 403, message = "É necessário estar autenticado para acessar o recurso"),
            @ApiResponse(code = 404, message = "Marca não encontrada"),
    })
    @GetMapping(value = "/{id}/patrimonios", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity buscarPatrimonios(@PathVariable String id) throws NavitaException {
        return ResponseEntity.ok(service.buscarPatrimoniosPorMarca(id));
    }

    @ApiOperation(value = "Salvar Marca", response = PatrimonioDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Criado com sucesso"),
            @ApiResponse(code = 400, message = "Requisição contém erros"),
            @ApiResponse(code = 403, message = "É necessário estar autenticado para acessar o recurso")
    })
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity salvar(@RequestBody @Valid MarcaRequestBody body,
                                 BindingResult result,
                                 final UriComponentsBuilder uriComponentsBuilder) throws FormException, NavitaException {

        if(result.hasErrors())
            throw new FormException(result.getFieldErrors());

        return Optional.ofNullable(service.salvar(body)).map(m ->
                ResponseEntity.created(uriComponentsBuilder.path("/marca/{id}")
                        .buildAndExpand(m.getId()).toUri()).body(m)).get();
    }

    @ApiOperation(value = "Alterar Marca", response = PatrimonioDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Alterado com sucesso"),
            @ApiResponse(code = 400, message = "Requisição contém erros"),
            @ApiResponse(code = 403, message = "É necessário estar autenticado para acessar o recurso"),
            @ApiResponse(code = 404, message = "Marca não encontrada"),
    })
    @PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity alterar(@PathVariable String id,
                                  @RequestBody @Valid MarcaRequestBody body,
                                  BindingResult result) throws FormException, NavitaException {

        if(result.hasErrors())
            throw new FormException(result.getFieldErrors());

       return ResponseEntity.ok(service.alterar(id, body));
    }

    @ApiOperation(value = "Deletar Marca", response = PatrimonioDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Deletado com sucesso"),
            @ApiResponse(code = 400, message = "Requisição contém erros"),
            @ApiResponse(code = 403, message = "É necessário estar autenticado para acessar o recurso"),
            @ApiResponse(code = 404, message = "Marca não encontrada"),
    })
    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity deletar(@PathVariable String id) throws NavitaException {
        service.deletar(id);
        return ResponseEntity.ok().build();
    }
}
