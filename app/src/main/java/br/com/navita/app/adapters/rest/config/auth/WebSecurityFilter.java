package br.com.navita.app.adapters.rest.config.auth;

import br.com.navita.app.core.application.dto.usuario.UsuarioTokenService;
import br.com.navita.app.core.domain.usuario.Usuario;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

public class WebSecurityFilter extends OncePerRequestFilter {

    private UsuarioTokenService tokenService;

    public WebSecurityFilter(final UsuarioTokenService tokenService){
        this.tokenService = tokenService;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
            throws ServletException, IOException {

        final Optional<Usuario> opUsuario = tokenService.validarToken(request);
        if(Boolean.FALSE.equals(opUsuario.isPresent())){
            filterChain.doFilter(request, response);
            return;
        }

        userAuthentication(opUsuario);
        filterChain.doFilter(request, response);
    }

    protected void userAuthentication(final Optional<Usuario> opUser){
        if(opUser.isPresent()){
            final Usuario usuario = opUser.get();
            final UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
                    new UsernamePasswordAuthenticationToken(usuario, null, usuario.getAuthorities());
            SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
        }
    }
}
