package br.com.navita.app.core.commons.security;

import org.owasp.esapi.ESAPI;
import org.owasp.esapi.crypto.PlainText;
import org.owasp.esapi.errors.EncryptionException;
import org.owasp.validator.html.*;
import org.springframework.core.io.ClassPathResource;

import java.io.InputStream;
import java.util.Objects;

public class CommonsSecurity {

    private static Policy policyAntiSamy;

    private CommonsSecurity(){}

    public static Boolean isHtmlSafe(final String value) {
        Boolean isSafe = Boolean.FALSE;

        try {
            AntiSamy antiSamy = new AntiSamy(lazyLoadPolicy());
            CleanResults scanned = antiSamy.scan(value);
            isSafe = (scanned.getNumberOfErrors() == 0);
        } catch (ScanException | PolicyException e) {}

        return isSafe;
    }

    public static String encryptor(final String value) throws EncryptionException {
        return ESAPI.encoder().encodeForBase64(
                ESAPI.encryptor().encrypt(new PlainText(value)).asPortableSerializedByteArray(), Boolean.FALSE);
    }

    private static Policy lazyLoadPolicy() throws PolicyException {
        if(Objects.isNull(policyAntiSamy)) {
            final ClassPathResource classPathResource = new ClassPathResource("/esapi/antisamy-esapi.xml");

            if(Boolean.FALSE.equals(Objects.isNull(classPathResource)) && Boolean.TRUE.equals(classPathResource.exists())) {
                InputStream is = CommonsSecurity.class.getResourceAsStream("/esapi/antisamy-esapi.xml");
                policyAntiSamy = Policy.getInstance(is);
            }
        }
        return policyAntiSamy;
    }
}
