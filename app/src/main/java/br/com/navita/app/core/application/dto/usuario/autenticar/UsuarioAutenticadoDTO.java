package br.com.navita.app.core.application.dto.usuario.autenticar;

import br.com.navita.app.core.domain.usuario.Usuario;

import java.io.Serializable;

public class UsuarioAutenticadoDTO implements Serializable {

    private  String id;
    private  String nome;
    private  UsuarioTokenDTO token;

    public static UsuarioAutenticadoDTO of(final String token, final String tipo, final Usuario usuario){
        UsuarioAutenticadoDTO dto = new UsuarioAutenticadoDTO();
        dto.setId(usuario.getId().toString());
        dto.setNome(usuario.getNome());
        dto.setToken(UsuarioTokenDTO.of(token, tipo));
        return dto;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public UsuarioTokenDTO getToken() {
        return token;
    }

    public void setToken(UsuarioTokenDTO token) {
        this.token = token;
    }
}
