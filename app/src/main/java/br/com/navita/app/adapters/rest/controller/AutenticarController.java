package br.com.navita.app.adapters.rest.controller;

import br.com.navita.app.adapters.rest.exception.FormException;
import br.com.navita.app.core.application.dto.patrimonio.PatrimonioDTO;
import br.com.navita.app.core.application.dto.usuario.autenticar.AutenticarService;
import br.com.navita.app.core.application.form.AutenticarRequestBody;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/autenticar")
public class AutenticarController {

    private final AutenticarService service;

    public AutenticarController(final AutenticarService service){
        this.service = service;
    }

    @ApiOperation(value = "Autenticar", response = PatrimonioDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Sucesso"),
            @ApiResponse(code = 400, message = "Requisição com erros")
    })
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity auth(@RequestBody @Valid AutenticarRequestBody request,
                               BindingResult result) throws FormException {

        if(result.hasErrors())
            throw new FormException(result.getFieldErrors());

        return ResponseEntity.ok(service.auth(request));
    }
}
