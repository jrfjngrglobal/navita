package br.com.navita.app.core.application.form;

import br.com.navita.app.core.commons.validadores.string.NavitaString;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

public class PatrimonioRequestBody implements Serializable {

    @NavitaString(required = true, fieldName = "nome", minLength = 2, maxLength = 50)
    @ApiModelProperty(notes = "Senha do Patrimônio", required = true, dataType = "java.lang.String")
    private String nome;

    @NavitaString(required = true, fieldName = "marcaId", isUUID = true)
    @ApiModelProperty(notes = "Identificador da Marca na qual o Patrimônio é vinculado", required = true, dataType = "java.lang.String")
    private String marcaId;

    @NavitaString(required = false, fieldName = "descricao", maxLength = 1000)
    @ApiModelProperty(notes = "Descrição do Patrimônio", dataType = "java.lang.String" )
    private String descricao;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getMarcaId() {
        return marcaId;
    }

    public void setMarcaId(String marcaId) {
        this.marcaId = marcaId;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }
}
