package br.com.navita.app.core.commons.validadores;

import javax.validation.ConstraintValidatorContext;

public class NavitaValidator {

    protected void buildMessage(ConstraintValidatorContext context,
                                final String message, final String fieldName){
        context.buildConstraintViolationWithTemplate(message)
                .addConstraintViolation()
                .disableDefaultConstraintViolation();
    }

    protected void buildMessage(ConstraintValidatorContext context, final String message){
        context.buildConstraintViolationWithTemplate(message)
                .addConstraintViolation()
                .disableDefaultConstraintViolation();
    }
}
