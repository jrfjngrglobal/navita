package br.com.navita.app.adapters.rest.controller;

import br.com.navita.app.adapters.rest.exception.FormException;
import br.com.navita.app.core.application.dto.patrimonio.PatrimonioDTO;
import br.com.navita.app.core.application.dto.usuario.UsuarioService;
import br.com.navita.app.core.application.exeption.NavitaException;
import br.com.navita.app.core.application.form.UsuarioRequestBody;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.util.Optional;

@CrossOrigin
@RestController
@RequestMapping("/usuario")
public class UsuarioController {

    private final UsuarioService service;

    public UsuarioController(final UsuarioService service){
        this.service = service;
    }

    @ApiOperation(value = "Buscar Todos Usuários", response = PatrimonioDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Sucesso"),
            @ApiResponse(code = 403, message = "É necessário estar autenticado para acessar o recurso")
    })
    @GetMapping( produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity get(){
        return ResponseEntity.ok(service.buscarTodos());
    }

    @ApiOperation(value = "Salvar Usuário", response = PatrimonioDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Criado com sucesso"),
            @ApiResponse(code = 400, message = "Requisição contém erros"),
            @ApiResponse(code = 403, message = "É necessário estar autenticado para acessar o recurso")
    })
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity post(@RequestBody @Valid UsuarioRequestBody body,
                               BindingResult result,
                               final UriComponentsBuilder uriComponentsBuilder) throws FormException, NavitaException {

        if(result.hasErrors())
            throw new FormException(result.getFieldErrors());

        return Optional.ofNullable(service.salvar(body)).map(usr ->
                ResponseEntity.created(uriComponentsBuilder.path("/usuario/{id}")
                        .buildAndExpand(usr.getId()).toUri()).body(usr)).get();
    }

    @ApiOperation(value = "Deletar Usuário", response = PatrimonioDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Deletado com sucesso"),
            @ApiResponse(code = 400, message = "Requisição contém erros"),
            @ApiResponse(code = 403, message = "É necessário estar autenticado para acessar o recurso"),
            @ApiResponse(code = 404, message = "Usuário não encontrado"),
    })
    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity delete(@PathVariable String id) throws NavitaException {
        service.deletar(id);
        return ResponseEntity.ok().build();
    }
}
