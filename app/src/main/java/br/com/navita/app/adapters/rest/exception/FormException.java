package br.com.navita.app.adapters.rest.exception;

import org.springframework.validation.FieldError;

import java.util.List;

public class FormException extends Exception {

    private final List<FieldError> errors;

    public FormException(final List<FieldError> errors) {
        this.errors = errors;
    }

    public List<FieldError> getErrors() {
        return errors;
    }
}