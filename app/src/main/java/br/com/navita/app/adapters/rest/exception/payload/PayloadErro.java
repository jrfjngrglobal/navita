package br.com.navita.app.adapters.rest.exception.payload;


import br.com.navita.app.core.application.commons.enums.ECodigoErro;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.validation.FieldError;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class PayloadErro {

    public int status;

    public List<ErroApi> erros;

    public static PayloadErro of(final int status, final String value, final ECodigoErro eCodigoErro){
        PayloadErro payload = new PayloadErro();
        payload.setStatus(status);
        payload.setErros(Arrays.asList(new ErroApi(value, eCodigoErro.name())));
        return payload;
    }

    public static PayloadErro of(final int  httpStatusCode, final ErroApi erro) {
        PayloadErro payload = new PayloadErro();
        payload.setStatus(httpStatusCode);
        payload.setErros(Arrays.asList(erro));
        return payload;
    }

    public static PayloadErro of(final List<FieldError> errors, final int httpStatusCode) {
        PayloadErro payload = new PayloadErro();
        payload.setStatus(httpStatusCode);

        Optional.ofNullable(errors).ifPresent(errorsList -> {
            payload.setErros(new ArrayList<>());
            errorsList.forEach(objectError -> {
                Object rejectedValue = Optional.ofNullable(objectError.getRejectedValue()).orElse(new Object());

                payload.getErros().add(new ErroApi(objectError.getField(), rejectedValue.toString(),
                        objectError.getDefaultMessage(), ECodigoErro.INVALID_VALUE));
            });
        });

        return payload;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public List<ErroApi> getErros() {
        return erros;
    }

    private void setErros(List<ErroApi> erros) {
        this.erros = erros;
    }
}
