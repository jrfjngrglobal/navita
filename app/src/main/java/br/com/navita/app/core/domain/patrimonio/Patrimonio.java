package br.com.navita.app.core.domain.patrimonio;

import br.com.navita.app.core.domain.marca.Marca;
import org.owasp.esapi.ESAPI;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;

@Entity
public class Patrimonio implements Serializable {

    @Id
    @GeneratedValue
    private UUID id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "marca_id")
    private Marca marca;

    private String nome;

    private String descricao;

    private Long numeroTombo;

    @CreatedDate
    private LocalDateTime dtCriacao;

    @CreatedDate
    private LocalDateTime dtUltimaAlteracao;

    @Version
    private Integer versao;

    public static Patrimonio of(final Marca marca, final String nome, final String descricao) {
        Patrimonio entidade = new Patrimonio();
        entidade.setMarca(marca);
        entidade.setNome(nome);
        entidade.setDescricao(descricao);
        entidade.setNumeroTombo(ESAPI.randomizer().getRandomLong());
        return entidade;
    }

    public void update(final Marca marca, final String nome, final String descricao){
        this.marca = marca;
        this.nome = nome;
        this.descricao = descricao;
    }

    @PrePersist
    protected void onCreate(){
        if(Objects.isNull(this.dtCriacao)){
            this.dtCriacao = LocalDateTime.now();
            this.dtUltimaAlteracao = LocalDateTime.now();
        }
    }

    @PreUpdate
    protected void onUpdate(){
        this.dtUltimaAlteracao = LocalDateTime.now();
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Marca getMarca() {
        return marca;
    }

    public void setMarca(Marca marca) {
        this.marca = marca;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Long getNumeroTombo() {
        return numeroTombo;
    }

    public void setNumeroTombo(Long numeroTombo) {
        this.numeroTombo = numeroTombo;
    }

    public LocalDateTime getDtCriacao() {
        return dtCriacao;
    }

    public void setDtCriacao(LocalDateTime dtCriacao) {
        this.dtCriacao = dtCriacao;
    }

    public LocalDateTime getDtUltimaAlteracao() {
        return dtUltimaAlteracao;
    }

    public void setDtUltimaAlteracao(LocalDateTime dtUltimaAlteracao) {
        this.dtUltimaAlteracao = dtUltimaAlteracao;
    }

    public Integer getVersao() {
        return versao;
    }

    public void setVersao(Integer versao) {
        this.versao = versao;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
