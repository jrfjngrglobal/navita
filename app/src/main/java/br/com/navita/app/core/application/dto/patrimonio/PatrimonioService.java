package br.com.navita.app.core.application.dto.patrimonio;

import br.com.navita.app.adapters.jpa.MarcaRepositoryJpaAdapter;
import br.com.navita.app.adapters.jpa.PatrimonioRepositoryJpaAdapter;
import br.com.navita.app.core.application.commons.enums.ECodigoErro;
import br.com.navita.app.core.application.commons.helpers.MessageHelper;
import br.com.navita.app.core.application.exeption.NavitaException;
import br.com.navita.app.core.application.form.PatrimonioRequestBody;
import br.com.navita.app.core.domain.marca.Marca;
import br.com.navita.app.core.domain.patrimonio.Patrimonio;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class PatrimonioService {

    private final PatrimonioRepositoryJpaAdapter patrimonioRepository;
    private final MarcaRepositoryJpaAdapter marcaRepository;
    private final MessageHelper messageHelper;

    public PatrimonioService(final PatrimonioRepositoryJpaAdapter patrimonioRepository,
                             final MarcaRepositoryJpaAdapter marcaRepository,
                             final MessageHelper messageHelpe){
        this.patrimonioRepository = patrimonioRepository;
        this.marcaRepository = marcaRepository;
        this.messageHelper = messageHelpe;
    }

    @Transactional
    @PreAuthorize("hasRole('ROLE_USUARIO')")
    public List<PatrimonioDTO> buscarTodas(){
        return patrimonioRepository.buscarTodas().stream().map(PatrimonioDTO::of).collect(Collectors.toList());
    }

    @Transactional
    @PreAuthorize("hasRole('ROLE_USUARIO')")
    public PatrimonioDTO buscarPorId(final String id) throws NavitaException {
        return PatrimonioDTO.of(buscarPatrimonio(id));
    }

    @Transactional
    @PreAuthorize("hasRole('ROLE_USUARIO')")
    public PatrimonioDTO salvar(final @Valid PatrimonioRequestBody body) throws NavitaException {
       return PatrimonioDTO.of(patrimonioRepository
               .salvarOuAlterar(Patrimonio.of(buscarMarca(body.getMarcaId()), body.getNome(), body.getDescricao())));
    }

    @Transactional
    @PreAuthorize("hasRole('ROLE_USUARIO')")
    public PatrimonioDTO alterar(final String id, final @Valid PatrimonioRequestBody body) throws NavitaException {
        Patrimonio entidade = buscarPatrimonio(id);
        entidade.update(buscarMarca(body.getMarcaId()), body.getNome(), body.getDescricao());
        return PatrimonioDTO.of(patrimonioRepository.salvarOuAlterar(entidade));
    }

    @Transactional
    @PreAuthorize("hasRole('ROLE_USUARIO')")
    public void deletar(final String id) throws NavitaException {
        Patrimonio entidade = buscarPatrimonio(id);
        patrimonioRepository.deletar(entidade);
    }

    @Transactional
    private Patrimonio buscarPatrimonio(final String id) throws NavitaException {
        return  patrimonioRepository.buscarPorId(UUID.fromString(id))
                .orElseThrow(() -> NavitaException.of("id", id, ECodigoErro.NOT_FOUND));
    }

    @Transactional
    private Marca buscarMarca(final String idMarca) throws NavitaException {
        return  marcaRepository.buscarPorId(UUID.fromString(idMarca))
                .orElseThrow(() -> NavitaException.of("id", idMarca, ECodigoErro.NOT_FOUND));
    }
}
