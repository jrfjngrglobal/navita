package br.com.navita.app.core.domain.usuario;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;
import java.util.UUID;

public interface UsuarioRepository extends JpaRepository<Usuario, UUID> {

    @Query("SELECT usr FROM Usuario usr WHERE usr.email = :email")
    Optional<Usuario> buscarPorEmail(final String email);
}