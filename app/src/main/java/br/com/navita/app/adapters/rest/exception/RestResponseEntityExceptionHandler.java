package br.com.navita.app.adapters.rest.exception;


import br.com.navita.app.adapters.rest.exception.payload.ErroApi;
import br.com.navita.app.adapters.rest.exception.payload.PayloadErro;
import br.com.navita.app.core.application.exeption.NavitaException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(FormException.class)
    protected ResponseEntity handle(final FormException ex) {
        return ResponseEntity.badRequest()
                .body(PayloadErro.of(ex.getErrors(), HttpStatus.BAD_REQUEST.value()));
    }

    @ExceptionHandler(NavitaException.class)
    protected ResponseEntity handle(final NavitaException ex) {
        return ResponseEntity.badRequest()
                .body(PayloadErro.of(ex.getCodigoErro().getCodigo(), new ErroApi(ex)));
    }
}