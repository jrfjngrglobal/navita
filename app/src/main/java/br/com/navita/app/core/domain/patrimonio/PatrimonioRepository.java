package br.com.navita.app.core.domain.patrimonio;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface PatrimonioRepository extends JpaRepository<Patrimonio, UUID> { }