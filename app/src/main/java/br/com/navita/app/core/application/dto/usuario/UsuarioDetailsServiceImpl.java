package br.com.navita.app.core.application.dto.usuario;

import br.com.navita.app.adapters.jpa.UsuarioRepositoryJpaAdapter;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UsuarioDetailsServiceImpl implements UserDetailsService {

    private final UsuarioRepositoryJpaAdapter usuarioRepository;

    public UsuarioDetailsServiceImpl(final UsuarioRepositoryJpaAdapter usuarioRepository){
        this.usuarioRepository = usuarioRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return usuarioRepository.buscarPorEmail(username).orElseThrow(() -> new UsernameNotFoundException(""));
    }
}
