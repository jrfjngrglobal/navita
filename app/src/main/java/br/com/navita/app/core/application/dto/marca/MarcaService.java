package br.com.navita.app.core.application.dto.marca;

import br.com.navita.app.adapters.jpa.MarcaRepositoryJpaAdapter;
import br.com.navita.app.core.application.commons.enums.ECodigoErro;
import br.com.navita.app.core.application.commons.helpers.MessageHelper;
import br.com.navita.app.core.application.dto.patrimonio.PatrimonioDTO;
import br.com.navita.app.core.application.exeption.NavitaException;
import br.com.navita.app.core.application.form.MarcaRequestBody;
import br.com.navita.app.core.domain.marca.Marca;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class MarcaService {

    private final MarcaRepositoryJpaAdapter marcaRepository;
    private final MessageHelper messageHelper;

    public MarcaService(final MarcaRepositoryJpaAdapter marcaRepository,
                        final MessageHelper messageHelpe){
        this.marcaRepository = marcaRepository;
        this.messageHelper = messageHelpe;
    }

    @Transactional
    @PreAuthorize("hasRole('ROLE_USUARIO')")
    public List<MarcaDTO> buscarTodas(){
        return marcaRepository.buscarTodas().stream().map(MarcaDTO::of).collect(Collectors.toList());
    }

    @Transactional
    @PreAuthorize("hasRole('ROLE_USUARIO')")
    public MarcaDTO buscarPorId(final String id) throws NavitaException {
        return MarcaDTO.of(buscarMarca(id));
    }

    @Transactional
    @PreAuthorize("hasRole('ROLE_USUARIO')")
    public List<PatrimonioDTO> buscarPatrimoniosPorMarca(final String id) throws NavitaException {
        return buscarMarca(id).getPatrimonios().stream().map(PatrimonioDTO::of).collect(Collectors.toList());
    }

    @Transactional
    @PreAuthorize("hasRole('ROLE_USUARIO')")
    public MarcaDTO salvar(final @Valid MarcaRequestBody body) throws NavitaException {
        validarMarcaPorNome(body.getNome(), Optional.empty());
        return MarcaDTO.of(marcaRepository.salvarOuAlterar(
                Marca.of(body.getNome())));
    }

    @Transactional
    @PreAuthorize("hasRole('ROLE_USUARIO')")
    public MarcaDTO alterar(final String id, final @Valid MarcaRequestBody body) throws NavitaException {
        Marca marca = buscarMarca(id);
        validarMarcaPorNome(body.getNome(), Optional.ofNullable(id));

        marca.setNome(body.getNome());
        return MarcaDTO.of(marcaRepository.salvarOuAlterar(marca));
    }

    @Transactional
    @PreAuthorize("hasRole('ROLE_USUARIO')")
    public void deletar(final String id) throws NavitaException {
        marcaRepository.deletar(buscarMarca(id));
    }

    @Transactional
    private Marca buscarMarca(final String id) throws NavitaException {
        return  marcaRepository.buscarPorId(UUID.fromString(id))
                .orElseThrow(() -> NavitaException.of("id", id, ECodigoErro.NOT_FOUND));
    }

    @Transactional
    private void validarMarcaPorNome(final String nome, final Optional<String> idMarca) throws NavitaException {
        Optional<Marca> opMarca = Optional.empty();

        if(Boolean.FALSE.equals(idMarca.isPresent()))
            opMarca =  marcaRepository.buscarMarcaPorNome(nome);
        else
            opMarca =  marcaRepository.buscarMarcaPorNome(nome, UUID.fromString(idMarca.get()));

        if(Boolean.TRUE.equals(opMarca.isPresent()))
            throw NavitaException.of("email", messageHelper.getMessage("nomeMarcaCadastrado", nome), nome, ECodigoErro.BRAND_ALREADY_REGISTERED);

    }
}
