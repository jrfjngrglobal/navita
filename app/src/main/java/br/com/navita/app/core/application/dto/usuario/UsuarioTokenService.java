package br.com.navita.app.core.application.dto.usuario;

import br.com.navita.app.adapters.jpa.UsuarioRepositoryJpaAdapter;
import br.com.navita.app.core.domain.usuario.Usuario;
import io.jsonwebtoken.*;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Optional;

@Service
public class UsuarioTokenService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UsuarioTokenService.class);

    @Value("${jwt.issuer}")
    private String issuer;

    @Value("${jwt.expiration}")
    private String expiration;

    @Value("${jwt.secret}")
    private String secret;

    @Value("${jwt.type}")
    private String type;

    private final UsuarioRepositoryJpaAdapter usuarioRepository;

    public UsuarioTokenService(final UsuarioRepositoryJpaAdapter usuarioRepository){
        this.usuarioRepository = usuarioRepository;
    }

    public String gerarToken(final Authentication authentication){
        final Usuario usuario = (Usuario) authentication.getPrincipal();
        return Jwts.builder()
                .setIssuer(issuer)
                .setSubject(getSubject(usuario))
                .setIssuedAt(new Date())
                .setExpiration(getExpirationDate())
                .signWith(SignatureAlgorithm.HS256, secret)
                .compact();
    }

    protected Optional<String> getHeaderToken(final HttpServletRequest request){
        final String token = request.getHeader("Authorization");

        if(!StringUtils.isEmpty(token) && token.startsWith(type.concat(" ")))
            return Optional.of(token.substring(type.length() + 1, token.length()));

        return Optional.empty();
    }

    public Optional<Usuario> validarToken(final HttpServletRequest request){
        final Optional<String> token = getHeaderToken(request);

        if(!token.isPresent())
            return Optional.empty();

        return searchBySubject(parseToken(token.get()));
    }

    public String parseToken(final String token){

        try{
            return Jwts.parser()
                    .setSigningKey(secret)
                    .parseClaimsJws(token)
                    .getBody().getSubject();

        }catch (ExpiredJwtException exception) {
            LOGGER.warn("Request to parse expired JWT : {} failed : {}", token, exception.getMessage());
        } catch (UnsupportedJwtException exception) {
            LOGGER.warn("Request to parse unsupported JWT : {} failed : {}", token, exception.getMessage());
        } catch (MalformedJwtException exception) {
            LOGGER.warn("Request to parse invalid JWT : {} failed : {}", token, exception.getMessage());
        } catch (SignatureException exception) {
            LOGGER.warn("Request to parse JWT with invalid signature : {} failed : {}", token, exception.getMessage());
        } catch (IllegalArgumentException exception) {
            LOGGER.warn("Request to parse empty or null JWT : {} failed : {}", token, exception.getMessage());
        }

        return null;
    }

    private Date getExpirationDate(){
        return new Date(System.currentTimeMillis() + Long.valueOf(expiration));
    }

    private String getSubject(final Usuario usuario){
       return usuario.getUsername();
    }

    private Optional<Usuario> searchBySubject(final String subject){
        return usuarioRepository.buscarPorEmail(subject);
    }
}
