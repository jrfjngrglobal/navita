package br.com.navita.app.core.application.dto.usuario.autenticar;

import br.com.navita.app.core.application.dto.usuario.UsuarioTokenService;
import br.com.navita.app.core.application.form.AutenticarRequestBody;
import br.com.navita.app.core.domain.usuario.Usuario;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Service;

@Service
public class AutenticarService {

    @Value("${jwt.type}")
    private String typeToken;

    private final AuthenticationManager authenticationManager;
    private final UsuarioTokenService tokenService;

    public AutenticarService(final AuthenticationManager authenticationManager,
                             final UsuarioTokenService tokenService){
        this.authenticationManager = authenticationManager;
        this.tokenService = tokenService;
    }

    public UsuarioAutenticadoDTO auth(final AutenticarRequestBody body) {
        try {
            final Authentication authentication = authenticationManager.authenticate(body.of());
            return UsuarioAutenticadoDTO.of(tokenService.gerarToken(authentication), typeToken, (Usuario) authentication.getPrincipal());
        }catch (AuthenticationException ex){

            throw ex;
        }
    }
}
