package br.com.navita.app.core.application.commons.enums;

public enum ECodigoErro {

    NOT_FOUND(404),
    INVALID_VALUE(400),
    EMAIL_ALREADY_REGISTERED(409),
    BRAND_ALREADY_REGISTERED(409);

    private final int codigo;

    ECodigoErro(final int codigo){
        this.codigo = codigo;
    }

    public int getCodigo() {
        return codigo;
    }
}