package br.com.navita.app.core.application.dto.marca;


import br.com.navita.app.core.domain.marca.Marca;

import java.io.Serializable;

public class MarcaDTO implements Serializable {

    private String id;
    private String nome;

    public static MarcaDTO of(final Marca marca){
        MarcaDTO dto = new MarcaDTO();
        dto.setId(marca.getId().toString());
        dto.setNome(marca.getNome());
        return dto;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
