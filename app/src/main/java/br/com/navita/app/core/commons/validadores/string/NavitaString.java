package br.com.navita.app.core.commons.validadores.string;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Target({FIELD})
@Retention(RUNTIME)
@Constraint(validatedBy = NavitaStringValidator.class)
public @interface NavitaString {
    String message() default "{malicioso}";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};

    String value() default "";
    String fieldName();
    boolean required() default false;
    boolean isEmail() default false;
    boolean isUUID() default false;
    int minLength() default -1;
    int maxLength() default -1;
}



