package br.com.navita.app.core.application.dto.usuario;

import br.com.navita.app.core.domain.usuario.Usuario;

import java.io.Serializable;

public class UsuarioDTO implements Serializable {

    private String id;
    private String nome;
    private String email;

    public static UsuarioDTO of(final Usuario usuario){
        UsuarioDTO dto = new UsuarioDTO();
        dto.setId(usuario.getId().toString());
        dto.setNome(usuario.getNome());
        dto.setEmail(usuario.getEmail());
        return dto;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
