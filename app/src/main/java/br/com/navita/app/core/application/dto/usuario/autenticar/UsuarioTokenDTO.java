package br.com.navita.app.core.application.dto.usuario.autenticar;

import java.io.Serializable;

public class UsuarioTokenDTO implements Serializable {

    private String token;
    private String tipo;

    public static UsuarioTokenDTO of(final String token, final String tipo){
        UsuarioTokenDTO dto = new UsuarioTokenDTO();
        dto.setToken(token);
        dto.setTipo(tipo);
        return dto;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
}
