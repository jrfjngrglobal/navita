package br.com.navita.app.core.commons.validadores.string;

import br.com.navita.app.core.commons.security.CommonsSecurity;
import br.com.navita.app.core.commons.validadores.NavitaValidator;
import org.apache.commons.lang3.StringUtils;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.errors.ValidationException;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.UUID;

public class NavitaStringValidator extends NavitaValidator implements ConstraintValidator<NavitaString, String> {

    private String fieldName;
    private boolean required;
    private boolean isEmail;
    private boolean isUUID;

    private int minLength;
    private int maxLength;

    @Override
    public void initialize(NavitaString constraintAnnotation) {
        this.fieldName = constraintAnnotation.fieldName();
        this.required = constraintAnnotation.required();
        this.isEmail = constraintAnnotation.isEmail();
        this.isUUID = constraintAnnotation.isUUID();

        this.minLength = constraintAnnotation.minLength();
        this.maxLength = constraintAnnotation.maxLength();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {

        // Required
        if(Boolean.TRUE.equals(required) && StringUtils.isBlank(value)){
            buildMessage(context,"{obrigatorio}", fieldName);
            return Boolean.FALSE;
        }

        if(StringUtils.isNotBlank(value)){

            if(Boolean.FALSE.equals(CommonsSecurity.isHtmlSafe(value))){
                buildMessage(context,"{malicioso}", fieldName);
                return Boolean.FALSE;
            }

            if(Boolean.TRUE.equals(isUUID)){
                try {
                    UUID.fromString(value);
                }catch (Exception ex){
                    buildMessage(context,"{invalidoFormatoUUID}", fieldName);
                    return Boolean.FALSE;
                }
            }

            if(minLength >= 0 || maxLength > 0){
                final int length = value.length();

                if(minLength >= 0 && maxLength > 0){
                    if(length < minLength || length > maxLength){
                        buildMessage(context,"{tamanhoMinimoMaximo}", fieldName);
                        return Boolean.FALSE;
                    }
                }
            }
        }

        if(Boolean.TRUE.equals(isEmail)){
            try{
                ESAPI.validator().getValidInput("Email Validator", value, "Email", 70, false);
            }catch (ValidationException ex){
                buildMessage(context,"{javax.validation.constraints.Email.message}", fieldName);
                return Boolean.FALSE;
            }
        }

        return Boolean.TRUE;
    }
}