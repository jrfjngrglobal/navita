package br.com.navita.app.adapters.jpa;

import br.com.navita.app.core.domain.patrimonio.Patrimonio;
import br.com.navita.app.core.domain.patrimonio.PatrimonioRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

@Repository
public class PatrimonioRepositoryJpaAdapter {

    private final PatrimonioRepository repository;

    public PatrimonioRepositoryJpaAdapter(final PatrimonioRepository repository){
        this.repository = repository;
    }

    public Patrimonio salvarOuAlterar(Patrimonio entidade){
        return repository.save(entidade);
    }

    public Collection<Patrimonio> buscarTodas(){
        return repository.findAll();
    }

    public Optional<Patrimonio> buscarPorId(final UUID id){
        return repository.findById(id);
    }

    public void deletar(final Patrimonio entidade){
        repository.delete(entidade);
    }
}
