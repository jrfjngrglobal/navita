package br.com.navita.app.core.application.form;

import br.com.navita.app.core.commons.validadores.string.NavitaString;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

public class MarcaRequestBody implements Serializable {

    @NavitaString(required = true, fieldName = "nome", minLength = 2, maxLength = 50)
    @ApiModelProperty(notes = "Nome da Marca", required = true, dataType = "java.lang.String")
    private String nome;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
