package br.com.navita.app.core.application.dto.usuario;

import br.com.navita.app.adapters.jpa.UsuarioRepositoryJpaAdapter;
import br.com.navita.app.core.application.commons.enums.ECodigoErro;
import br.com.navita.app.core.application.commons.enums.EFuncao;
import br.com.navita.app.core.application.commons.helpers.MessageHelper;
import br.com.navita.app.core.application.exeption.NavitaException;
import br.com.navita.app.core.application.form.UsuarioRequestBody;
import br.com.navita.app.core.domain.usuario.Usuario;
import br.com.navita.app.core.domain.usuario.UsuarioFuncao;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class UsuarioService {

    private final UsuarioRepositoryJpaAdapter usuarioRepository;
    private final MessageHelper messageHelper;

    public UsuarioService(final UsuarioRepositoryJpaAdapter usuarioRepository,
                          final MessageHelper messageHelper){
        this.usuarioRepository = usuarioRepository;
        this.messageHelper = messageHelper;
    }

    @Transactional
    public UsuarioDTO salvar(final @Valid UsuarioRequestBody body) throws NavitaException {
        final Optional<Usuario> opUsuario =
                usuarioRepository.buscarPorEmail(body.getEmail());

        if(Boolean.TRUE.equals(opUsuario.isPresent()))
            throw NavitaException.of("email", messageHelper.getMessage("emailJaCadastrado", body.getEmail()), body.getEmail(), ECodigoErro.EMAIL_ALREADY_REGISTERED);

        Usuario usuario = Usuario.of(body.getNome(), body.getEmail(), body.getSenha());
        usuario.addFuncoes(UsuarioFuncao.of(EFuncao.ROLE_USUARIO, usuario));
        return UsuarioDTO.of(usuarioRepository.salvarOuAlterar(usuario));
    }

    @Transactional
    @PreAuthorize("hasRole('ROLE_USUARIO')")
    public List<UsuarioDTO> buscarTodos(){
        return usuarioRepository.buscarTodos().stream().map(UsuarioDTO::of).collect(Collectors.toList());
    }

    @Transactional
    @PreAuthorize("hasRole('ROLE_USUARIO')")
    public void deletar(final String idUsuario) throws NavitaException {
        final Optional<Usuario> opUsuario =
                usuarioRepository.buscarPorId(UUID.fromString(idUsuario));

        if(Boolean.FALSE.equals(opUsuario))
            throw NavitaException.of("id", idUsuario, ECodigoErro.NOT_FOUND);

        usuarioRepository.deletar(opUsuario.get());
    }
}
