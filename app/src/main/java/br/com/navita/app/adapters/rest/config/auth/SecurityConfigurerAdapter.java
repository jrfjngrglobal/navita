package br.com.navita.app.adapters.rest.config.auth;

import br.com.navita.app.core.application.dto.usuario.UsuarioDetailsServiceImpl;
import br.com.navita.app.core.application.dto.usuario.UsuarioTokenService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.header.writers.StaticHeadersWriter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfigurerAdapter extends WebSecurityConfigurerAdapter {

    private final UsuarioTokenService tokenService;
    private final UsuarioDetailsServiceImpl usuarioDetailsService;

    public SecurityConfigurerAdapter(final UsuarioTokenService tokenService,
                                     final UsuarioDetailsServiceImpl usuarioDetailsService) {
        this.tokenService = tokenService;
        this.usuarioDetailsService = usuarioDetailsService;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        addHeader(http);

        http.cors().and().csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and().addFilterBefore(new WebSecurityFilter(tokenService), UsernamePasswordAuthenticationFilter.class);

        http
                .authorizeRequests()
                .antMatchers(HttpMethod.POST, "/autenticar").permitAll()
                .antMatchers(HttpMethod.POST, "/usuario").permitAll()
                .and()
                .authorizeRequests()
                .anyRequest().authenticated();
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(usuarioDetailsService).passwordEncoder(passwordEncoder());
    }

    @Override
    public void configure(WebSecurity web) {
        web.ignoring()
                .antMatchers("/**.html", "/v2/api-docs", "/webjars/**", "/configuration/**", "/swagger-resources/**")
                .antMatchers(HttpMethod.OPTIONS);
    }

    @Override
    @Bean
    protected AuthenticationManager authenticationManager() throws Exception {
        return super.authenticationManager();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    protected void addHeader(HttpSecurity http) throws Exception{
        http
                .headers().httpStrictTransportSecurity()
                .and()
                .frameOptions().deny()
                .contentSecurityPolicy("frame-ancestors 'none'")
                .and()
                .addHeaderWriter(
                        new StaticHeadersWriter("Feature-Policy",
                                "autoplay 'none'; camera 'none'; microphone 'none'; speaker 'none'; usb 'none'"));
    }
}
