package br.com.navita.app.core.domain.usuario;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.*;

@Entity
public class Usuario implements UserDetails {

    @Id
    @GeneratedValue
    private UUID id;

    private String nome;

    private String email;

    private String senha;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "usuario")
    private Set<UsuarioFuncao> funcoes;

    @CreatedDate
    private LocalDateTime dtCriacao;

    @CreatedDate
    private LocalDateTime dtUltimaAlteracao;

    @Version
    private Integer versao;

    public static Usuario of(final String nome, final String email,
                             final String senha){
        Usuario entidade = new Usuario();
        entidade.setNome(nome);
        entidade.setEmail(email);
        entidade.setSenha(new BCryptPasswordEncoder().encode(senha));
        return entidade;
    }

    @PrePersist
    protected void onCreate(){
        if(Objects.isNull(this.dtCriacao)){
            this.dtCriacao = LocalDateTime.now();
            this.dtUltimaAlteracao = LocalDateTime.now();
        }
    }

    @PreUpdate
    protected void onUpdate(){
        this.dtUltimaAlteracao = LocalDateTime.now();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return funcoes;
    }

    @Override
    public String getPassword() {
        return this.senha;
    }

    @Override
    public String getUsername() {
        return this.email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return Boolean.TRUE;
    }

    @Override
    public boolean isAccountNonLocked() {
        return Boolean.TRUE;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return Boolean.TRUE;
    }

    @Override
    public boolean isEnabled() {
        return Boolean.TRUE;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public Set<UsuarioFuncao> getFuncoes() {
        return funcoes;
    }

    public void setFuncoes(Set<UsuarioFuncao> funcoes) {
        this.funcoes = funcoes;
    }

    public LocalDateTime getDtCriacao() {
        return dtCriacao;
    }

    public void setDtCriacao(LocalDateTime dtCriacao) {
        this.dtCriacao = dtCriacao;
    }

    public LocalDateTime getDtUltimaAlteracao() {
        return dtUltimaAlteracao;
    }

    public void setDtUltimaAlteracao(LocalDateTime dtUltimaAlteracao) {
        this.dtUltimaAlteracao = dtUltimaAlteracao;
    }

    public Integer getVersao() {
        return versao;
    }

    public void setVersao(Integer versao) {
        this.versao = versao;
    }

    public void addFuncoes(UsuarioFuncao... funcoes){
        this.setFuncoes(new HashSet<>(Arrays.asList(funcoes)));
    }
}
