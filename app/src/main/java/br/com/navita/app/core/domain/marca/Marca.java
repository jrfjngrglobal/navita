package br.com.navita.app.core.domain.marca;

import br.com.navita.app.core.domain.patrimonio.Patrimonio;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

@Entity
public class Marca implements Serializable {

    @Id
    @GeneratedValue
    private UUID id;

    private String nome;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "marca", orphanRemoval=true)
    private Set<Patrimonio> patrimonios;

    @CreatedDate
    private LocalDateTime dtCriacao;

    @CreatedDate
    private LocalDateTime dtUltimaAlteracao;

    @Version
    private Integer versao;

    public static Marca of(final String nome){
        Marca entidade = new Marca();
        entidade.setNome(nome);
        return entidade;
    }

    @PrePersist
    protected void onCreate(){
        if(Objects.isNull(this.dtCriacao)){
            this.dtCriacao = LocalDateTime.now();
            this.dtUltimaAlteracao = LocalDateTime.now();
        }
    }

    @PreUpdate
    protected void onUpdate(){
        this.dtUltimaAlteracao = LocalDateTime.now();
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public LocalDateTime getDtCriacao() {
        return dtCriacao;
    }

    public void setDtCriacao(LocalDateTime dtCriacao) {
        this.dtCriacao = dtCriacao;
    }

    public LocalDateTime getDtUltimaAlteracao() {
        return dtUltimaAlteracao;
    }

    public void setDtUltimaAlteracao(LocalDateTime dtUltimaAlteracao) {
        this.dtUltimaAlteracao = dtUltimaAlteracao;
    }

    public Integer getVersao() {
        return versao;
    }

    public void setVersao(Integer versao) {
        this.versao = versao;
    }

    public Set<Patrimonio> getPatrimonios() {
        return patrimonios;
    }

    public void setPatrimonios(Set<Patrimonio> patrimonios) {
        this.patrimonios = patrimonios;
    }
}