package br.com.navita.app.adapters.jpa;

import br.com.navita.app.core.domain.usuario.Usuario;
import br.com.navita.app.core.domain.usuario.UsuarioRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

@Repository
public class UsuarioRepositoryJpaAdapter {

    private final UsuarioRepository repository;

    public UsuarioRepositoryJpaAdapter(final UsuarioRepository repository){
        this.repository = repository;
    }

    public Optional<Usuario> buscarPorEmail(final String email){
        return repository.buscarPorEmail(email);
    }

    public Optional<Usuario> buscarPorId(final UUID id){
        return repository.findById(id);
    }

    public void deletar(final Usuario entidade){
        repository.delete(entidade);
    }

    public Collection<Usuario> buscarTodos(){
        return repository.findAll();
    }

    public Usuario salvarOuAlterar(Usuario entidade){
        return repository.save(entidade);
    }
}