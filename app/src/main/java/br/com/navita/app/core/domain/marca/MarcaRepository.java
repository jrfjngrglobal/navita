package br.com.navita.app.core.domain.marca;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;
import java.util.UUID;

public interface MarcaRepository extends JpaRepository<Marca, UUID> {

    @Query("SELECT m FROM Marca m WHERE UPPER(m.nome) = UPPER(:nome)")
    Optional<Marca> buscarMarcaPorNome(final String nome);

    @Query("SELECT m FROM Marca m WHERE UPPER(m.nome) = UPPER(:nome) AND m.id <> :diferenteDe")
    Optional<Marca> buscarMarcaPorNome(final String nome, final UUID diferenteDe);
}