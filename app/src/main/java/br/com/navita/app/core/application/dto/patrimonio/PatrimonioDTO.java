package br.com.navita.app.core.application.dto.patrimonio;

import br.com.navita.app.core.domain.patrimonio.Patrimonio;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class PatrimonioDTO implements Serializable {

    private String id;
    private String nome;
    
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String descricao;
    private Long numeroTombo;

    public static PatrimonioDTO of(final Patrimonio entidade){
        PatrimonioDTO dto = new PatrimonioDTO();
        dto.setId(entidade.getId().toString());
        dto.setNome(entidade.getNome());
        dto.setDescricao(entidade.getDescricao());
        dto.setNumeroTombo(entidade.getNumeroTombo());
        return dto;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Long getNumeroTombo() {
        return numeroTombo;
    }

    public void setNumeroTombo(Long numeroTombo) {
        this.numeroTombo = numeroTombo;
    }
}
