package br.com.navita.app.core.application.form;

import br.com.navita.app.core.commons.validadores.string.NavitaString;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;

import java.io.Serializable;

public class AutenticarRequestBody implements Serializable {

    @NavitaString(required = true, fieldName = "email", minLength = 2, maxLength = 70)
    @ApiModelProperty(notes = "Email de Acesso", required = true, dataType = "java.lang.String")
    private String email;

    @NavitaString(required = true, fieldName = "senha", minLength = 8, maxLength = 16)
    @ApiModelProperty(notes = "Senha", required = true, dataType = "java.lang.String")
    private String senha;

    public UsernamePasswordAuthenticationToken of(){
        return new UsernamePasswordAuthenticationToken(getEmail(), getSenha());
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
}
