package br.com.navita.app.core.application.dto.familia;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


@SpringBootTest
public class FamiliaNumeroTest {

    private final FamiliaNumeroService service = new FamiliaNumeroService();

    @Test
    public void sucesso() throws Exception {
        assertEquals(553, service.obterMaiorNumeroDaFamilia(553l));
    }

    @Test
    public void quando_excede_valor() throws Exception {
        assertEquals(-1, service.obterMaiorNumeroDaFamilia(7999757989l));
    }

    @Test
    public void numero_invalido() {
        assertThrows(Exception.class, () -> service.obterMaiorNumeroDaFamilia(-1l));
    }
}
